import express from "express"

import {message} from "./cd/a"

const app = express()

app.get("/",(req,res)=>{
    res.send(message)
    // res.redirect("google.com")
})

app.listen(3000,()=>{
    console.log("App is listening...");
})
